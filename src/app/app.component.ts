import { Component, ViewChild, ElementRef, OnInit, NgZone } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('canvas', { static: true }) canvas: ElementRef<HTMLCanvasElement>;
  ctx: CanvasRenderingContext2D;
  h: Number;
  w: Number;
  square;
  perimeter;
  resultElement;
  btn;
  rect;
  scale;
  resultArr;
  constructor(private ngZone: NgZone) {}

  ngOnInit() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.rect = this.canvas.nativeElement.getBoundingClientRect()
    this.scale = window.devicePixelRatio
    this.canvas.nativeElement.height = this.rect.height * this.scale
    this.canvas.nativeElement.width = this.rect.width * this.scale
    this.canvas.nativeElement.addEventListener('mousedown', this.clickHandler.bind(this))
    this.btn = document.querySelector('button')
    this.btn.addEventListener('click', this.showSquareAndPerimetr.bind(this))
    this.resultArr = []
    this.square = 0
    this.perimeter = 0
    this.resultElement = document.querySelector('.result')
  }

  clickHandler(e: MouseEvent) {
    this.ctx.arc(e.offsetX, e.offsetY, 2, 0, Math.PI * 2)
    this.ctx.fill()
    this.resultArr.push([e.offsetX, e.offsetY])
  }

  showSquareAndPerimetr() {
    for (let i = 0; i < this.resultArr.length-1; i++) {
      this.square += this.resultArr[i][0]*this.resultArr[i+1][1]-this.resultArr[i][1]*this.resultArr[i+1][0]
      this.perimeter += Math.sqrt((this.resultArr[i+1][0]-this.resultArr[i][0])**2+(this.resultArr[i+1][1]-this.resultArr[i][1])**2)
    }
    this.resultElement.innerHTML = `Square: ${Math.abs(this.square/2)}; Perimetr: ${Math.trunc(this.perimeter)}`
  }

  ngOnDestroy() {
    
  }
}
